# wsservice-k8s-deploy

`wsservice`镜像被我放到`https://cloud.docker.com/u/liuhl/repository/docker/liuhl/ifoc.wsservice`上

## 我的环境
- **k8s部署环境**: 本地虚拟机搭建的集群,1 master + 2 nodes
- **k8s版本**: v1.14.0,之前是v1.13.1,昨天被我升级为:v1.14.0
- **k8scni插件**: `weave`或`flannel`
- **LoadBalancer Provider**: metallb
- **Ingress**: k8s 维护的[ingress-nginx](https://github.com/kubernetes/ingress-nginx) 和nginx社区维护的[ingress-nginx](https://github.com/nginxinc/kubernetes-ingress)都尝试过

- **docker版本**: docker ce 18.09.3

源码稍后给到，因为我们是通过nuget.server维护基础组件包的,单独把ws应用抽出来的话需要整理一下代码

## 部署 
 
需要先启动中间件才可以部署应用:
```
sudo kubectl apply -f consul.yml -f rabbitmq.yml -f redis.yml
sudo kubectl apply -f ifoc-conf-dev.yml -f wsservices-deployments.yml -f wsservices-services.yml -f wsservices-ingress-dev.yml
```



该应用没有使用到数据库,所以数据库连接不会对应用的启动有影响。

## 存在问题
wssocket无法通过ingress路由进行握手。

如果通过`service`暴露出的ip和端口进行握手是没有问题的,如下下图:

![1](images/1.png)

如果将service type的设置的类型设置为:`NodePort`，通过映射的ip和端口就无法进行握手,如下图:

![2](images/2.png)

通过 nginx-ingress 进行握手也是不行的,如下图:

![3](images/3.png)

## 如何建立握手
ws://ip:port/v1/releaseeval?flightId=1232